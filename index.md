---
title: Benji's Slide Decks
---

## https://benjifisher.gitlab.io/slide-decks/

- [Handling HTML Markup with Drupal's Migrate API](./html-migrate-api.html) (TBD)
- [DevOps for Presentations: Markdown, Pandoc, Reveal.js, GitLab CI](./devops-slides.html) (May 22, 2019)
- [Caching Large Navigation Menus in Drupal](./caching-nerds-2019.html) (March 10, 2019)
- [Drupal+Gatsby Quick Start with Lando](./lgd.html) (March 5, 2019)
- [The State of the Migrate API in Drupal 8](./migration.html) (February 5, 2019)
- [Caching Large Navigation Menus](./meetup-2018-12-04.html) (December 4, 2018)
- [Pandoc and Reveal.js](./meetup-2018-09-04.html) (September 4, 2018)
- [Example for Pandoc and Reveal.js](./example.html) (August 5, 2018)
